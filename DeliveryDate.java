package assignment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DeliveryDate {
	int duration;
	String ordereddate;

	public static void main(String[] args){
		DeliveryDate sd = new DeliveryDate("03-11-1999 15:00:00", 17);
        System.out.println("Shipment arrives on " + sd.getDate());
	}
	public static LocalDateTime getLocalDateTimeInUTC(){
	    ZonedDateTime nowUTC = ZonedDateTime.now(ZoneOffset.UTC);

	    return nowUTC.toLocalDateTime();
	}
	
	public String getDate(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(this.ordereddate, formatter);
        
        int timetaken = this.duration;
        int numberOfHrsPerDay = PerDayConstraints.HOURS.numberOfHrsPerDay;

        CheckConstraints check = new CheckConstraints();
        
        while( true ){
        	if(!check.isHoliday(dateTime)){
        		int remainingHrs = this.remainingHrsInTheDay(dateTime);
        		int hrsToAdd = Math.min(remainingHrs, numberOfHrsPerDay);
        		hrsToAdd = Math.min(hrsToAdd, timetaken);
        		dateTime = dateTime.plusHours(hrsToAdd);
        		timetaken -= hrsToAdd;
        	}
        	if(timetaken > 0)
        		dateTime = this.nextDayMidnight(dateTime);
        	else
        		break;
        }
        String currTime = dateTime.toLocalTime().toString();
        String day = dateTime.getDayOfWeek().name().toLowerCase();
        return day + " " + currTime;
	}

	
	
	public DeliveryDate(String ordereddate, int duration){
		this.duration = duration;
		this.ordereddate = ordereddate;
	}
	
	
	
	public int remainingHrsInTheDay(LocalDateTime dateTime){
		int hours = (int)dateTime.until( nextDayMidnight(dateTime), ChronoUnit.HOURS );
		return hours;
	}

	
	
	public LocalDateTime nextDayMidnight(LocalDateTime dateTime){
		LocalTime midnight = LocalTime.MIDNIGHT;
		LocalDate date = dateTime.toLocalDate().plusDays(1);
		return LocalDateTime.of(date, midnight);
	}
}