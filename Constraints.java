package assignment;

import java.util.Arrays;
import java.util.List;

public class Constraints {

}
enum Holidays{
	HOLIDAYS("saturday", "sunday");

	private final List<String> values;

	Holidays(String ...values) {
		this.values = Arrays.asList(values);
	}

	public List<String> getValues() {
		return values;
	}
}

enum PublicHolidays{
	PUBLIC_HOLIDAYS("january 1", "january 26", "august 15", "may 1","december 25");
	private final List<String> values;

	PublicHolidays(String ...values) {
		this.values = Arrays.asList(values);
	}

	public List<String> getValues() {
		return values;
	}
}
enum PerDayConstraints{
	HOURS(12);
    int numberOfHrsPerDay;
    PerDayConstraints(int hrs){
    	this.numberOfHrsPerDay = hrs;
    }
}

