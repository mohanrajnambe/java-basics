package assignment;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DeliveryDateTest {

	@Test
	public void testNormalDays() {
		DeliveryDate sd = new DeliveryDate("22-09-2020 10:00:00", 30);
		assertEquals("thursday 06:00", sd.getDate());			
	}
	
	@Test
	public void testHolidays() {
		DeliveryDate sd = new DeliveryDate("26-09-2020 07:05:40", 27);
		assertEquals("wednesday 03:00", sd.getDate());			
	}

	@Test
	public void testPublicHolidays() {
		// january 1st
		DeliveryDate sd = new DeliveryDate("01-01-2020 13:09:10", 45);
		assertEquals("monday 11:00", sd.getDate());	
		
		//august 15th
		sd = new DeliveryDate("15-08-2020 19:15:00", 14);
		assertEquals("tuesday 02:00", sd.getDate());
		
		//january 26th
		sd = new DeliveryDate("26-01-2020 23:11:00", 39);
		assertEquals("thursday 03:00", sd.getDate());	
	}
	
	@Test
	public void testLeapYear(){
		DeliveryDate sd = new DeliveryDate("28-02-2020 10:00:00", 40);
		assertEquals("wednesday 04:00", sd.getDate());	
	}
	
	@Test
	public void testY2K(){
		DeliveryDate sd = new DeliveryDate("31-12-1999 15:00:00", 17);
		assertEquals("monday 08:00", sd.getDate());	
	}
	
	
	
}
